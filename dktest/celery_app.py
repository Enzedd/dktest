# coding=utf-8
import logging
import os

from celery import Celery

__author__ = 'Niyaz.Batyrshin'
logger = logging.getLogger(__name__)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dktest.settings')

app = Celery('dktest')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

# @setup_logging.connect
# def setup_celery_logging(sender=None, **kwargs):
#     import logging.config
#     logging_config = settings.LOGGING
#
#     if settings.SYSLOG_HANDLER:  # replace file_handler with syslog
#         logging_config['handlers']['syslog'] = settings.SYSLOG_HANDLER
#         if 'file_handler' in logging_config['handlers']:
#             del logging_config['handlers']['file_handler']
#         for l in logging_config['loggers'].values():
#             if 'file_handler' in l.get('handlers', []):
#                 l['handlers'].remove('file_handler')
#                 l['handlers'].append('syslog')
#                 l['formatter'] = 'syslog'
#
#     logging.config.dictConfig(logging_config)
#     logging.getLogger('nextcert.task').debug('Celery logging configured')
