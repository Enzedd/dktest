# coding=utf-8
import logging

from celery import current_app

__author__ = 'Niyaz.Batyrshin'
logger = logging.getLogger(__name__)


@current_app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
