from django.urls import path

from demo.views import debug_task_view, ping_view

urlpatterns = [
    path('ping/', ping_view),
    path('debug-task/', debug_task_view),
]
