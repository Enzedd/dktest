from django.http import JsonResponse

# Create your views here.
from demo.tasks import debug_task


def ping_view(request):
    return JsonResponse({'value': 'pong'})


def debug_task_view(request):
    debug_task.apply_async()
    return JsonResponse({'status': 'ok'})
