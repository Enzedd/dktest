from django.test import TestCase


# Create your tests here.
class TestPing_view(TestCase):
    def test_ping_view(self):
        response = self.client.get('demo/ping/')
        self.assertEqual(response.status_code, 200)
